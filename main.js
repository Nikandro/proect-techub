// function add() {
//     var para = document.createElement("TD");
//     document.getElementById("myTD").appendChild(para);
//   };

  

//   function remove() {
//     var list = document.getElementById("myTD");
//     list.removeChild(list.lastChild);
//   };



// function add() {
//   var para = document.createElement("TD");
//   var a = document.createElement("INPUT");
//   para.append(a);
//   [...document.querySelectorAll("tr")].forEach(tr => tr.append(para.cloneNode(true)))

  
  
// };

 

function add() {
  //   create input
  var newInput = document.createElement("INPUT");
  var para = document.createElement("TD");
  para.append(newInput);
  //   create header
  var newTh = document.createElement("TD");
  var addedTime = new Date().toISOString().substring(0, 10);
  newTh.innerHTML = `<b>Time/${addedTime}</b>`
 
  //   add header
  document.querySelectorAll("thead tr")[0].appendChild(newTh)

  //   add rows
  document.querySelectorAll("tbody tr").forEach(tr => tr.append(para.cloneNode(true)));
};

 

function remove() {
    [...document.querySelectorAll("tr td:last-child")].forEach(td => td.remove());
}